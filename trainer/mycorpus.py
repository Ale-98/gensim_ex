import os

from gensim import utils
import gsmutils
import PyPDF2

MIN_LENGTH_WORDS = 4


class MyCorpus(object):
    """An interator that yields sentences (lists of str)."""

    def __init__(self, data_path):
        self.data_path = data_path
        print('Instantiated MyCorpus')

    def __iter__(self):
        pdf = open(self.data_path, 'rb')
        readable_pdf = PyPDF2.PdfFileReader(pdf)
        num_pages = readable_pdf.getNumPages()
        for i in range(2, num_pages):
            # assume there's one document per line, tokens separated by whitespace
            yield utils.simple_preprocess(readable_pdf.getPage(i).extractText())


class PDFCorpus(object):
    """An interator that yields sentences (lists of pdf pages)."""

    def __init__(self, data_path):
        self.data_path = data_path
        print('Instantiated MyCorpus')

    def __iter__(self):
        for file_name in os.listdir(self.data_path):
            pdf = open(os.path.join(self.data_path, file_name), 'rb')
            readable_pdf = PyPDF2.PdfFileReader(pdf)
            num_pages = readable_pdf.getNumPages()
            for i in range(2, num_pages):
                # assume there's one document per line, tokens separated by whitespace
                yield utils.simple_preprocess(readable_pdf.getPage(i).extractText())


def remove_stopwords(line):
    cleaned = [token for token in line if token not in gsmutils.default_stoppers]
    return cleaned


def remove_shortwords(line):
    cleaned = [token for token in line if len(token) > MIN_LENGTH_WORDS]
    return cleaned
