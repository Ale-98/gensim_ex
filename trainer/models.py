from django.db import models


# Create your models here.
class Documents(models.Model):
    title = models.CharField(max_length=50)
    text = models.CharField(max_length=10000)

    def __str__(self):
        return self.title+'\n'+self.text