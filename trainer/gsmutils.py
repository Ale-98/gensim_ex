import re
import tempfile
from collections import defaultdict
import numpy as np
from gensim import corpora, utils, similarities, models

# Defining stoplist elements
preps = 'di a da in con su per tra fra dal del dello delle della dalle dalla degli dallo dagli dall\' ' \
        'dell\' nello negli nella nelle nel nell\' sullo sugli allo agli al all\' '
articles = 'il lo la i gli le un uno una l\' '
cong = 'e ed a ad cui che '
prons = 'io tu egli lui lei noi voi essi loro '
others = 'questo quello quegli questi quelle quelli quell\' si no non '
punt = ', ; : ( ) \' \" '

# Defining stoplist
default_stoppers = set((preps + articles + cong + prons + others + punt).split(' '))
# Defining minimum value to say if two words are similar to each other
MIN_TO_BE_SIMILAR = 0.5


def clean_string(text):
    re_text = re.sub('[^0-9a-zA-Z]+', ' ', text)
    return re_text


def return_only_ascii(text):
    return ''.join([x for x in text if x.isascii()])


def doc2tokens(document):
    # [token for token in document.lower().split(' ') if token not in punt]
    return utils.simple_preprocess(document)


def create_corpus(doc, stoppers):
    """Takes a text document and a list of stopwords and produces
    a dictionary containing dictionary(key=dictionary) and corpus(key=corpus) made
    with the lines of the document"""
    documents = doc.readlines()
    docs = [utils.simple_preprocess(line) for line in documents]
    # Remove stoppers
    texts = [[token for token in line if token not in stoppers] for line in docs]

    # Remove low frequency tokens
    frequency = defaultdict(int)
    for line in texts:
        for token in line:
            frequency[token] += 1

    texts = [[token for token in line if frequency[token] > 1] for line in texts]

    # Creating dictionary and corpus
    dictionary = corpora.Dictionary(texts)
    corpus = [dictionary.doc2bow(text) for text in texts]
    return {
        'dictionary': dictionary,
        'corpus': corpus,
        'documents': documents
    }


def compute_lsi(model, corpus, dictionary, input, top_n=10):
    # Usare similarities.Similarity: consente di aggiungere altri documenti all'index in seguito
    index = similarities.MatrixSimilarity(model[corpus])

    vec_input = dictionary.doc2bow(input.lower().split(' '))
    lsi_input = model[vec_input]

    sims = index[lsi_input]
    sims = sorted(enumerate(sims), key=lambda item: -item[1])
    result = []

    for i, s in enumerate(sims):
        if i == top_n:
            break
        result.append(s)

    return result


def word2vec_sims(model, input_word, top_n=10):
    result = []
    try:
        words = utils.simple_preprocess(input_word)
        similar_to_word = model.wv.most_similar(positive=words, topn=top_n)
        print('=== FOUND THESE SIMILARITIES ===')
        for word, rate in similar_to_word:
            result.append('' + word + ': ' + str(np.fix(rate * 100)) + '%')
    except KeyError:
        print('Word %s not in vocabulary' % input_word)

    return result


def most_similar_tag(model, input_word, tags):
    """Returns the most similar tag in tags to input_word"""
    sims = []
    sims_dict = {}
    for tag in tags:
        similarity = model.similarity(input_word, tag)
        if similarity > MIN_TO_BE_SIMILAR:
            sims_dict[similarity] = tag
            sims.append(similarity)

    # If almost one of the tags is enough similar to input_word
    if len(sims) > 0:
        # Sorting in descending order and getting the most similar
        sims.sort(reverse=True)
        return sims_dict[sims[0]]

    else:
        return None


def temporarily_save_w2vmodel(corpus):
    """Saves a Word2Vec model in the filepath temporarily and return the directory where it will be stored"""
    model = models.Word2Vec(sentences=corpus)
    with tempfile.NamedTemporaryFile(prefix='gensim-model-', delete=False) as tmp:
        temporary_filepath = tmp.name
        model.save(temporary_filepath)
    return temporary_filepath


def load_w2vmodel(path):
    """Loads model from given path"""
    return models.Word2Vec.load(path)


def update_model(corpus, model):
    """Updates the given model with the given corpus"""
    model.build_vocab(corpus, update=True)
    model.train(corpus, total_examples=model.corpus_count, epochs=model.iter)
