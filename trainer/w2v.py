import gsmutils

import gensim.models
import gsmutils
import logging

import mycorpus

# Enabling logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

# Opening a document
url = '../docs/signore_delle_mosche.txt'
urlpdf = '../docs/signore_delle_mosche_total_book.pdf'
url2pdf = '../docs/interpretazione_dei_sogni_total_book.pdf'
# document = open(url, 'r', encoding='utf-8')

# demo corpus 'lee_background.cor'
# data = gsmutils.create_corpus(document, gsmutils.default_stoppers)

# corpora.MmCorpus.serialize(fname='../docs/signore_delle_mosche.mm', corpus=data['corpus'], id2word=data['dictionary'])
# datapath = os.path.abspath('../docs/signore_delle_mosche.mm')

# corpus = mycorpus.MyCorpus(url2pdf)
pdfcorpus = mycorpus.PDFCorpus('../docs/')
temp_url = gsmutils.temporarily_save_w2vmodel(corpus=pdfcorpus)
print(temp_url)

model = gsmutils.load_w2vmodel(path=temp_url)
# To update model if new corpus is added to corpuses directory
# gsmutils.update_model(corpus=corpus, model=model)

for i, word in enumerate(model.wv.vocab):
    print(i, word)

while True:
    word = input('Insert a word: ')
    if word == 'exit':
        break
    print(list(gsmutils.word2vec_sims(model, word)))

print('You correctly exit the program!')
