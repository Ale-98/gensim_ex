from django.http import HttpResponse, JsonResponse
from django.views import generic
from .models import Documents
from . import gsmutils


# Create your views here.
class Index(generic.ListView):
    template_name = 'index.html'
    context_object_name = 'documents'
    queryset = Documents.objects.all


def similarities(request, input):
    document = Documents.objects.get(pk=input)

    tags = ['persona', 'tipo', 'materiale', 'animale']
    # This list of pairs will contain every word of document associated with the tag most similar to it
    similarity_pairs = []
    # Tokenizing document
    tokens = gsmutils.doc2tokens(document.text)
    # Getting model(rendere globale)
    model = gsmutils.load_w2vmodel(path='loaded C:\\Users\\simpl\\AppData\\Local\\Temp\\gensim-model-4vug4uqj')
    for token in tokens:
        most_similar_tag = gsmutils.most_similar_tag(model=model, input_word=token, tags=tags)
        similarity_pairs.append([token, most_similar_tag])

    json_document = {
        'title': document.title,
        'text': similarity_pairs
    }
    return JsonResponse(json_document)
