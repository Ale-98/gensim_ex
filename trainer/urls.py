from django.urls import path
from . import views

app_name = 'trainer'
urlpatterns = [
    path('', views.Index.as_view(), name='index'),
    path('<str:input>/similarities/', views.similarities, name='similarities')
]