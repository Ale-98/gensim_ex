import os
import re
import sys
from collections import defaultdict

from gensim import models, similarities

# Defining stoplist elements
import gsmutils

# Opening a document
url = '../docs/signore_delle_mosche.txt'
document = open(url, 'r', encoding='utf-8')

# Creating corpus
result = gsmutils.create_corpus(document, gsmutils.default_stoppers)
dictionary = result['dictionary']
corpus = result['corpus']

lsi = models.LsiModel(corpus=corpus, id2word=dictionary, num_topics=10)

# Usare similarities.Similarity: consente di aggiungere altri documenti all'index in seguito
input_word = input('Insert a word or a statement: ')

# Computing similarities
sims = gsmutils.compute_lsi(lsi, corpus, dictionary, input_word)
documents = result['documents']

for s in sims:
    print(s, documents[int(s[0])])
